if [ $1 -eq 1 ]
then
# no symbols

	date
	rm *.o *.ps launch || true
	g++ -std=c++14 -c -I../src ../src/math/MyMath.cpp
	g++ -std=c++14 -c -I../src ../src/mutex/MyMutex.cpp
	g++ -std=c++14 -c -I../src ../src/smartpointers/MySmartPointers.cpp
	g++ -std=c++14 -c -I../src ../src/smartpointers/SimpleClass.cpp
	g++ -std=c++14 -c -I../src ../src/thread/MyThread.cpp

	g++ -std=c++14 -c -I../src ../src/main.cpp


	g++  main.o MyThread.o MyMutex.o MyMath.o SimpleClass.o MySmartPointers.o -lpthread -o launch
	./launch
elif [ $1 -eq 2 ] 
then	
# with symbols
	date
	rm *.o *.ps launch || true
	g++ -std=c++14 -c -I../src -DARUN_ALL_EXAMPLES ../src/math/MyMath.cpp
	g++ -std=c++14 -c -I../src -DRUN_ALL_EXAMPLES ../src/mutex/MyMutex.cpp
	g++ -std=c++14 -c -I../src -DARUN_ALL_EXAMPLES ../src/smartpointers/MySmartPointers.cpp
	g++ -std=c++14 -c -I../src -DARUN_ALL_EXAMPLES ../src/smartpointers/SimpleClass.cpp
	g++ -std=c++14 -c -I../src -DARUN_ALL_EXAMPLES ../src/thread/MyThread.cpp

	g++ -std=c++14 -c -I../src -DRUN_ALL_EXAMPLES -DNRUN_THREAD_EXAMPLES ../src/main.cpp


	g++  main.o MyThread.o MyMutex.o MyMath.o SimpleClass.o MySmartPointers.o -lpthread -o launch
	./launch
fi

