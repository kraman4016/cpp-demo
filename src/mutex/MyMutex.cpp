
#include "MyMutex.h"
#include "common.h"

#include <iostream>
#include <iomanip>      // std::setfill, std::setw
#include <thread>

MyMutex::MyMutex() {
	FUNCTION_BEGIN

#ifdef RUN_ALL_EXAMPLES
	mutex();
	timedMutex();
	recursiveMutex();
#endif

	FUNCTION_END
}

// Parameterized Constructior
MyMutex::MyMutex(int nb) {
	FUNCTION_BEGIN
	
	FUNCTION_END
}


MyMutex::~MyMutex() {
	FUNCTION_BEGIN
	
	FUNCTION_END
}


void MyMutex::mutex() {
	FUNCTION_BEGIN

	// Attempt to lock a mutex,that has been already owned by calling thread. 
	// It results Undefined behaviour(Implementation depedent)	
	mtx1.lock();
	PRINT_THREAD_INFO("mtx1: in critical section")
	//mtx1.lock(); // deadlock: thread execution not proceeded further
	mtx1.unlock();

	// Attempts to unlock a mutex, that hasn't locked.	
	mtx1.unlock();
	mtx1.unlock();
	PRINT_THREAD_INFO("mtx1: unlock() called in non-sychronized sequence")
	
	// Attempt to unlock a mutex, that locked by other thread 
	auto anonymousFunction1 = [this] { 
	        mtx1.lock();
		PRINT_THREAD_INFO("mtx1: locked")
		PRINT_THREAD_INFO("sleep for 5 seconds")
	        std::this_thread::sleep_for(std::chrono::seconds(5));
	};
	auto anonymousFunction2 = [this] {
		PRINT_THREAD_INFO("sleep for 5 seconds")
	        std::this_thread::sleep_for(std::chrono::seconds(5));
		PRINT_THREAD_INFO("mtx1: Attempt to unlock the mutex that has owned by another thread")
        	mtx1.unlock();
	        //mtx1.lock(); // infinite block
	};
	std::thread t1(anonymousFunction1);
	std::thread t2(anonymousFunction2);
	t1.join();
	t2.join();
	
	FUNCTION_END
}


void MyMutex::timedMutex() {
	FUNCTION_BEGIN

	PRINT_THREAD_INFO("tmtx1: Attempt to lock")
	tmtx1.lock();
	PRINT_THREAD_INFO("tmtx1: locked state")
	tmtx1.unlock();
	PRINT_THREAD_INFO("tmtx1: unlocked state")
	PRINT_THREAD_INFO("tmtx1: Attempt to lock using try_lock_for() expression with 05 seconds timeout")
	tmtx1.try_lock_for(std::chrono::seconds(5));
	PRINT_THREAD_INFO("tmtx1: locked state")
	PRINT_THREAD_INFO("tmtx1: Attempt to lock using try_lock_for() expression with 10 seconds timeout")
	bool lockStatus = tmtx1.try_lock_for(std::chrono::seconds(10));
	PRINT_THREAD_INFO(std::boolalpha)
	PRINT_THREAD_INFO("tmtx1: try_lock_for() expression return status:")
	
	FUNCTION_END
}


void MyMutex::recursiveMutex() {
	FUNCTION_BEGIN

	PRINT_THREAD_INFO("rtmtx1: Attempt to lock")
	rmtx1.lock();
	PRINT_THREAD_INFO("rtmtx1: locked state")

	rmtx1.unlock();
	PRINT_THREAD_INFO("rtmtx1: unlocked state")
	
        // Attempt to unlock & lock a recursive mutex, that is locked by other thread 
        auto anonymousFunction1 = [this] {
                rmtx1.lock();
		PRINT_THREAD_INFO("rmtx1: locked")
		PRINT_THREAD_INFO("sleep for 10 seconds")
                std::this_thread::sleep_for(std::chrono::seconds(10));
        };

        auto anonymousFunction2 = [this] {
		PRINT_THREAD_INFO("sleep for 5 seconds")
                std::this_thread::sleep_for(std::chrono::seconds(05));
		PRINT_THREAD_INFO("rmtx1: Attempt to unlock,that has owned by another thread")
                rmtx1.unlock();
		PRINT_THREAD_INFO("rmtx1: Attempt to lock, that has owned by another thread")
                //rmtx1.lock(); // infinite block
        };
        std::thread t1(anonymousFunction1);
        std::thread t2(anonymousFunction2);
        t1.join();
        t2.join();

	// check matching number of lock & unlock calls
        auto anonymousFunction3 = [this] {
                rmtx1.lock();
		PRINT_THREAD_INFO("rmtx1: locked")
		PRINT_THREAD_INFO("sleep for 10 seconds")
                std::this_thread::sleep_for(std::chrono::seconds(10));
        };

        auto anonymousFunction4 = [this] {
		PRINT_THREAD_INFO("sleep for 05 seconds")
                std::this_thread::sleep_for(std::chrono::seconds(05));
		PRINT_THREAD_INFO("rmtx1: Attempt to unlock,that has owned by another thread")
                rmtx1.unlock();
		PRINT_THREAD_INFO("rmtx1: Attempt to lock, that has owned by another thread")
                //rmtx1.lock(); // infinite block
        };
/*
        std::thread t3(anonymousFunction3);
        std::thread t4(anonymousFunction4);
        t3.join();
        t4.join();
*/	
	FUNCTION_END
}
