#ifndef MYMUTEX_H
#define MYMUTEX_H

#include <condition_variable>
#include <mutex>
#include <vector>

namespace Demo {

class MyMutex {
	public: 
		MyMutex();
		MyMutex(int nb);
		virtual ~MyMutex();
		void mutex();
		void timedMutex();
		void recursiveMutex();
		
	private:
		std::vector<std::string> vs;
		std::condition_variable cv;
		std::mutex mtx1;
		std::mutex mtx2;		
		std::recursive_mutex rmtx1;
		std::timed_mutex tmtx1;
};


}

using Demo::MyMutex;
#endif /* MYMUTEX_H */


