#ifndef MYSMARTPOINTERS_H
#define MYSMARTPOINTERS_H

#include <memory>

#include "mutex/MyMutex.h"
#include "SimpleClass.h"

namespace Demo {

class MySmartPointers {
	public: 
		MySmartPointers();
		virtual ~MySmartPointers();
		void sharedPointers();
		void passByValue(std::shared_ptr<MyMutex> shp);
		void passByReference(std::shared_ptr<MyMutex>& shp);
		void passStoredObject(SimpleClass sc);
		void passByValue(std::shared_ptr<SimpleClass> shsc);
	private:
};


}

using Demo::MySmartPointers;
#endif /* MYSMARTPOINTERS_H */


