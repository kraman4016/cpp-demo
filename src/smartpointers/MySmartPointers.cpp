
#include "MySmartPointers.h"
#include "common.h"
#include "SimpleClass.h"
#include "mutex/MyMutex.h"

#include <iostream>
#include <iomanip>      // std::setfill, std::setw


MySmartPointers::MySmartPointers() {
	FUNCTION_BEGIN

#ifdef RUN_ALL_EXAMPLES
	sharedPointers();
#endif

	FUNCTION_END
}


MySmartPointers::~MySmartPointers() {
	FUNCTION_BEGIN 

	FUNCTION_END
}


void MySmartPointers::sharedPointers() {
	FUNCTION_BEGIN
	
	// array of shared pointers
	SCOPE_BEGIN(1) 
	{
		std::shared_ptr<MyMutex> shp;
		shp = std::make_shared<MyMutex>(); // 1st instance
		std::vector<std::shared_ptr<MyMutex>> vshp;

		vshp.push_back(shp);
		vshp.clear(); // destructor not called
		EXAMPLE(1)
		
		vshp.push_back(std::move(shp));
		vshp.clear(); // destructor called
		EXAMPLE(2)
		
		vshp.push_back(std::shared_ptr<MyMutex>()); // object not created
		vshp.clear(); 
		EXAMPLE(3)

		vshp.push_back(std::shared_ptr<MyMutex>(new MyMutex)); // object created
		vshp.clear(); 
		EXAMPLE(4)
	}
	SCOPE_END(1) 

	// shared pointer intialization
	SCOPE_BEGIN(2)
	{
		std::shared_ptr<MyMutex> shp1 = std::make_shared<MyMutex>(); // more efficient in creating shared pointer
		EXAMPLE(1)

		std::shared_ptr<MyMutex> shp2(); // object not created
		EXAMPLE(2)

		std::shared_ptr<MyMutex> shp3(new MyMutex()); // atlease two memory allocations & can lead to memory leak
		EXAMPLE(3)

		passByValue(shp3);
		EXAMPLE(4)

		passByReference(shp1);
		EXAMPLE(5)

		std::shared_ptr<SimpleClass> shsc1 = std::make_shared<SimpleClass>();
		passByValue(shsc1);
		EXAMPLE(6)

		std::shared_ptr<SimpleClass> shsc2 = std::make_shared<SimpleClass>();
		passStoredObject(*shsc1); // destructor called after function scope
		EXAMPLE(7)

		std::shared_ptr<SimpleClass> shsc3 = std::make_shared<SimpleClass>();
		passStoredObject(*shsc3.get()); // destructor called after function scope
		EXAMPLE(8)
	}
	SCOPE_END(2) 

	FUNCTION_END
}


void MySmartPointers::passStoredObject(SimpleClass sc) {
	FUNCTION_BEGIN

	FUNCTION_END
}


void MySmartPointers::passByValue(std::shared_ptr<SimpleClass> shsc) {
	FUNCTION_BEGIN

	FUNCTION_END
}


void MySmartPointers::passByValue(std::shared_ptr<MyMutex> shp) {
	FUNCTION_BEGIN

	FUNCTION_END
}


void MySmartPointers::passByReference(std::shared_ptr<MyMutex>& shp) {
	FUNCTION_BEGIN

	FUNCTION_END
}
