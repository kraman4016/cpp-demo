#ifndef SIMPLECLASS_H
#define SIMPLECLASS_H


namespace Demo {

class SimpleClass {
	public: 
		SimpleClass();
		virtual ~SimpleClass();
	private:
};


}

using Demo::SimpleClass;
#endif /* SIMPLECLASS_H */


