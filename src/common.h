
#ifndef FUNCTION_BEGIN
	#define FUNCTION_BEGIN 		std::cout << "< FUNB >" << "\tF " << std::setfill(' ') << std::setw(20) << __func__ << "\t" << std::setfill(' ') << std::setw(10) << "L " << __LINE__ << "\n";
#endif


#ifndef FUNCTION_END
	#define FUNCTION_END  		std::cout << "< FUNE >" << "\tF " << std::setfill(' ') << std::setw(20) << __func__ << "\t" << std::setfill(' ') << std::setw(10) << "L " << __LINE__ << "\n";
#endif


#ifndef SCOPE_BEGIN 
	#define SCOPE_BEGIN(number)   	std::cout << "< SCPB >" << "\tS" << number << "\tF " << std::setfill(' ') << std::setw(20) << __func__ << "\t" << std::setfill(' ') << std::setw(10) << "L " << __LINE__ << "\n";
#endif


#ifndef SCOPE_END 
	#define SCOPE_END(number)	std::cout << "< SCPE >" << "\tS" << number << "\tF " << std::setfill(' ') << std::setw(20) << __func__ << "\t" << std::setfill(' ') << std::setw(10) << "L " << __LINE__ << "\n";
#endif


#ifndef EXAMPLE
	#define EXAMPLE(number)  	std::cout << "< EXMP >" << "\tE" << number << "\t" << __func__ << "\t" << __LINE__ << "\n";
#endif


#ifndef PRINT_INFO
	#define PRINT_INFO(arg)  	std::cout << "< INFO >" << "\t\t" << arg << "\n";
#endif


#ifndef PRINT_THREAD_INFO
	#define PRINT_THREAD_INFO(arg) 	{ std::mutex mt; mt.lock(); std::cout << "< TINF >" << "\t" << arg << "\t" << std::this_thread::get_id() << "\tF " << std::setfill(' ') << std::setw(30) << __func__ << "\t" << std::setfill(' ') << std::setw(10) << "L "<< __LINE__ << "\n"; 	mt.unlock();} 
#endif






