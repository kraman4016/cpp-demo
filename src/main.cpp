#include "common.h"
#include "thread/MyThread.h"
#include "mutex/MyMutex.h"
#include "math/MyMath.h"
#include "smartpointers/MySmartPointers.h"

#include <iostream>
#include <iomanip>      // std::setfill, std::setw


int main() {
	FUNCTION_BEGIN
	PRINT_INFO("Bonjour !")
	
	#if defined(RUN_ALL_EXAMPLES) || defined(RUN_THREAD_EXAMPLES)
	SCOPE_BEGIN(1)
	{		
		MyThread th;
	}
	SCOPE_END(1) 
	#endif

	#if defined(RUN_ALL_EXAMPLES) || defined(RUN_MUTEX_EXAMPLES)
	SCOPE_BEGIN(2)
	{		
		MyMutex mtx;
	}
	SCOPE_END(2) 
	#endif

	#if defined(RUN_ALL_EXAMPLES) || defined(RUN_SMARTPOINTERS_EXAMPLES)
	SCOPE_BEGIN(3)
	{		
		MySmartPointers sp;
	}
	SCOPE_END(3) 
	#endif

	#if defined(RUN_ALL_EXAMPLES) || defined(RUN_MATH_EXAMPLES)
	SCOPE_BEGIN(4)
	{		
		MyMath ma;
	}
	SCOPE_END(4) 
	#endif

	FUNCTION_END
	return 0;
}
