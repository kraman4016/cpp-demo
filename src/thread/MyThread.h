#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <condition_variable>
#include <mutex>

namespace Demo {

class MyThread {
	public: 
		MyThread();
		virtual ~MyThread();
	   	void displayNumbersUsingMultipleThreads();
		void printSequenceOfOddNumbers(int maxNb,uint32_t& maxNb2);
		void printSequenceOfEvenNumbers(int maxNb, uint32_t& maxNb2);
	private:
		std::condition_variable cv;
		std::mutex mtx;
		bool done;
};


}

using Demo::MyThread;
#endif /* MYTHREAD_H */


