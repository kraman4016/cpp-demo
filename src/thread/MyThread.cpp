#include "MyThread.h"
#include "common.h"

#include <iostream>
#include <iomanip>      // std::setfill, std::setw
#include <thread>
#include <atomic>

MyThread::MyThread() : done(false) {
	FUNCTION_BEGIN

#ifdef RUN_ALL_EXAMPLES
	displayNumbersUsingMultipleThreads();
#endif

	FUNCTION_END
}


MyThread::~MyThread() {
	FUNCTION_BEGIN

	FUNCTION_END
}


void MyThread::displayNumbersUsingMultipleThreads() {
	FUNCTION_BEGIN

	uint32_t maxNb2 = 15; 

	std::thread t1{&MyThread::printSequenceOfOddNumbers, this, 10, std::ref(maxNb2)};
	std::thread t2{&MyThread::printSequenceOfEvenNumbers, this, 10, std::ref(maxNb2)};
	t1.join();
	t2.join();

	FUNCTION_END
}


void MyThread::printSequenceOfOddNumbers(int maxNb, uint32_t& maxNb2) {
	FUNCTION_BEGIN	

	// use local variable to increment
	for (int i = 1; i <= maxNb2; i++) {
		if ((i % 2) == 1) {
                        std::unique_lock<std::mutex> lck(mtx);
			cv.wait(lck, [this]{ return not done;});
			PRINT_THREAD_INFO(i)
			done = true;
			cv.notify_one();
		}
	}

	FUNCTION_END
}


void MyThread::printSequenceOfEvenNumbers(int maxNb, uint32_t& maxNb2) {
	FUNCTION_BEGIN	

	// use local variable to increment
	for (int i = 1; i <= maxNb2; i++) {
		if ((i % 2) == 0) {
                        std::unique_lock<std::mutex> lck(mtx);
                        cv.wait(lck, [this]{ return  done;});
			PRINT_THREAD_INFO(i)
			done = false;
			cv.notify_one();
                }
        }

	FUNCTION_END	
}

