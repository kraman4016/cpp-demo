#include "common.h"
#include "MyMath.h"

#include <iostream>
#include <iomanip>      // std::setfill, std::setw
#include <cstring> 	// for srccpy

MyMath::MyMath() {
	FUNCTION_BEGIN

#ifdef RUN_ALL_EXAMPLES
	uint32_t nb = 123456;
	countOddEvenDigitsInInteger(nb);
	nb = 101102103;
	countOddEvenDigitsInInteger(nb);
	std::string str = "Function printf";
	reverseString(str);
	conversionToUppercase(str);
	memoryLeakCheck();
#endif

	FUNCTION_END
}


MyMath::~MyMath() {
	FUNCTION_BEGIN

	FUNCTION_END
}


void MyMath::countOddEvenDigitsInInteger(uint32_t nb) {
	FUNCTION_BEGIN

	PRINT_INFO("Interger: " << nb)
	int oddCount = 0;
	int evenCount = 0;

	while (nb > 0) {
		int lastDigit = nb % 10;
		nb = nb / 10;
		if ( (lastDigit % 2) == 0 ) {
			evenCount++;
			PRINT_INFO("even digit: " << lastDigit)
		} else {
			oddCount++;
			PRINT_INFO("odd digit: " << lastDigit)
		}	
	}

	PRINT_INFO("even count: " << evenCount)
	PRINT_INFO("odd count: " << oddCount)

	FUNCTION_END
}


void MyMath::reverseString(std::string str) {
	FUNCTION_BEGIN

	PRINT_INFO("Input String: " << str)
	int length = str.length() - 1;
	std::string reversedString;
	for (int i = length; i >= 0 ; i--) {
		char ch = str.at(i);
		reversedString += ch;
	}
	PRINT_INFO("reversed String: " << reversedString)

	FUNCTION_END
}


void MyMath::conversionToUppercase(std::string str) {
	FUNCTION_BEGIN

	PRINT_INFO("Input String: " << str)
	int length = str.length();
	std::string caseModifiedString;
	uint8_t caseConversionKey = ('a' - 'A');

	for (int i = 0; i < length ; i++) {
		char ch = str.at(i);
		if (ch >= 'a' && ch <= 'z') {
			//caseModifiedString += (ch - 32);
			caseModifiedString += (ch - caseConversionKey);
		}
		else {
			//caseModifiedString += ch;
			caseModifiedString.append(1, ch);
		}
	}
	PRINT_INFO("caseModifiedString: " << caseModifiedString)
	
	FUNCTION_END
}


void MyMath::memoryLeakCheck() {
	FUNCTION_BEGIN

	char* cptr = (char *)malloc(sizeof(char) * 5);
	strcpy(cptr, "123456789123456789");
	PRINT_INFO("heap memory occupied: " << strlen(cptr) << "\tactual allocated: " << (sizeof(char)*5) << "\tvalue: " << cptr)	

	char* cptr1 = (char *)malloc(sizeof(char) * 5);
	strcpy(cptr1, "123456789123456789");
	PRINT_INFO("heap memory occupied: " << strlen(cptr) << "\tactual allocated: " << (sizeof(char)*5) << "\tvalue: " << cptr1)	
	
	free(cptr);
	free(cptr1);

	FUNCTION_END

}

