#ifndef MYMATH_H
#define MYMATH_H


#include <iostream>

namespace Demo {

class MyMath {
	public: 
		MyMath();
		virtual ~MyMath();
		void countOddEvenDigitsInInteger(uint32_t nb);
		void reverseString(std::string str);
		void conversionToUppercase(std::string str);
		void memoryLeakCheck();
	private:
};


}

using Demo::MyMath;
#endif /* MYMATH_H */


