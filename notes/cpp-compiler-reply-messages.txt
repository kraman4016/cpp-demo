
# std::cout used outside function
	../src/mutex/MyMutex.cpp:16:7: error: ‘cout’ in namespace ‘std’ does not name a type
	  std::cout << FORMAT_FUNCTION_NAME; 

# semicolon missing
	../src/mutex/MyMutex.h:22:24: error: expected ‘;’ at end of member declaration
	   std::recursive_mutex rmtx1

# casting missing
	../src/math/MyMath.cpp:89:21: error: invalid conversion from ‘void*’ to ‘char*’ [-fpermissive]
  	char* cptr = malloc(sizeof(char) * 5); 

# template argument type mismatch
	../src/smartpointers/MySmartPointers.cpp: In member function ‘bool Demo::MySmartPointers::sharedPointers()’:
	../src/smartpointers/MySmartPointers.cpp:29:30: error: type/value mismatch at argument 1
		in template parameter list for ‘template<class _Tp, class _Alloc> class std::vector’
	   std::vector<std::shared_ptr> vshp;
				      ^
	../src/smartpointers/MySmartPointers.cpp:29:30: note:   expected a type, got ‘shared_ptr’
	../src/smartpointers/MySmartPointers.cpp:29:30: error: template argument 2 is invalid
	../src/smartpointers/MySmartPointers.cpp:30:8: error: request for member ‘push_back’ in ‘vshp’, which is of non-class type ‘int’
	   vshp.push_back(shp);

# mutex(member variable of class) is not copyable	
	../src/smartpointers/MySmartPointers.cpp: In member function ‘bool Demo::MySmartPointers::passByValue(std::shared_ptr<Demo::MyMutex>)’:
	../src/smartpointers/MySmartPointers.cpp:85:11: error: use of deleted function ‘Demo::MyMutex::MyMutex(const Demo::MyMutex&)’
	  test(*shp);
		   ^
	In file included from ../src/smartpointers/MySmartPointers.h:6:0,
		         from ../src/smartpointers/MySmartPointers.cpp:2:
	../src/smartpointers/../mutex/MyMutex.h:10:7: note: ‘Demo::MyMutex::MyMutex(const Demo::MyMutex&)’ 
		is implicitly deleted because the default definition would be ill-formed:
	 class MyMutex {
	       ^
	../src/smartpointers/../mutex/MyMutex.h:10:7: error: use of deleted function 
		‘std::condition_variable::condition_variable(const std::condition_variable&)’
	In file included from ../src/smartpointers/../mutex/MyMutex.h:4:0,
		         from ../src/smartpointers/MySmartPointers.h:6,
		         from ../src/smartpointers/MySmartPointers.cpp:2:
	/usr/include/c++/5/condition_variable:81:5: note: declared here
	     condition_variable(const condition_variable&) = delete;
	     ^
	In file included from ../src/smartpointers/MySmartPointers.h:6:0,
		         from ../src/smartpointers/MySmartPointers.cpp:2:
	../src/smartpointers/../mutex/MyMutex.h:10:7: error: use of deleted function ‘std::mutex::mutex(const std::mutex&)’
	 class MyMutex {
	       ^
	In file included from /usr/include/c++/5/condition_variable:39:0,
		         from ../src/smartpointers/../mutex/MyMutex.h:4,
		         from ../src/smartpointers/MySmartPointers.h:6,
		         from ../src/smartpointers/MySmartPointers.cpp:2:
	/usr/include/c++/5/mutex:129:5: note: declared here
	     mutex(const mutex&) = delete;

